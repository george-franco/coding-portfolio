import self from "../img/self.png"
import create_expense from "../img/create_expense.gif"
import carcar from "../img/carcar.png"
import projectmanager from "../img/projectmanager.png"


/* Hi there! Thanks for checking out my portfolio template. Be sure to read the comments to get a better understanding of
how to make this template work best for you! */

export let colors = ["rgb(0,255,164)", "rgb(166,104,255)"];
/*
I highly recommend using a gradient generator like https://gradientgenerator.paytonpierce.dev/ to generate a pair of colors that you like.
These colors will be used to style your name on the homepage, the background of your picture, and some other accents throughout
the site.
 */


/*
So let's get started! Some of the info below is pretty self-explanatory, like 'firstName' and 'bio'. I'll try to explain anything
that might not be obvious right off the bat :) I recommend looking at the template example live using "npm start" to get an idea
of what each of the values mean.
 */

export const info = {
    firstName: "George",
    lastName: "Franco",
    position: "a Full Stack Developer actively looking for work",
    selfPortrait: self, // don't change this unless you want to name your self-portrait in the "img" folder something else!
    gradient: `-webkit-linear-gradient(135deg, ${colors})`, // don't change this either
    baseColor: colors[0],
    miniBio: [ // these are just some "tidbits" about yourself. You can look at mine https://paytonjewell.github.io/#/ for an example if you'd like
        {
            emoji: '🌎',
            text: 'based in Silver Spring, MD'
        },
        {
            emoji: "📧",
            text: "gh.franco.98@gmail.com"
        }
    ],
    socials: [
        {
            link: "https://gitlab.com/george-franco",
            icon: "fa fa-gitlab",
            label: 'gitlab'
        },
        {
            link: "https://www.linkedin.com/in/george-franco/",
            icon: "fa fa-linkedin",
            label: 'linkedin'
        },
    ],
    skills:
        {
            proficientWith: ['python', 'javascript', 'react', 'fastapi', 'django', 'SQL', 'git', 'gitlab', 'bootstrap', 'html5', 'css3'],
        }
    ,
    portfolio: [ // This is where your portfolio projects will be detailed
        {
            title: "ExpenseBook",
            source: "https://gitlab.com/george-franco/expensebook", // this should be a link to the **repository** of the project, where the code is hosted.
            image: create_expense,
            live: "https://expensebook.onrender.com/"
        },
        {
            title: "CarCar",
            source: "https://gitlab.com/george-franco/CarCar",
            image: carcar
        },
        {
            title: "Project Manager",
            source: "https://gitlab.com/george-franco/project-alpha-apr",
            image: projectmanager
        },
        // {
        //     title: "NFL Injury Tracker",
        //     source: "https://gitlab.com/george-franco/football-injury-tracker",
        //     image: mock4
        // }
    ],
}